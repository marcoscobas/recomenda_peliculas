from django.apps import AppConfig


class RecomendaPeliculasConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "recomenda_peliculas"
